# LaTeX

<!-- 
https://github.com/PacktPublishing/LaTeX-Beginner-s-Guide/tree/master
-->

## Getting started

```latex
\documentclass[a4paper,11pt]{article}
\begin{document}
\title{Example 2}
\author{My name}
\date{January 5, 2011}
\maketitle
\section{What's this?}
This is our second document. It contains a title and a section
with text.
\end{document}
```


```latex
\documentclass[a4paper,11pt]{article}
\begin{document}
\title{Example 3}
\author{My name}
\date{January 5, 2011}
\maketitle
\section{What's this?}
This is our
second document.
It contains two paragraphs. The first line of a paragraph will be
indented, but not when it follows a heading.
% Here's a comment.
\end{document}
```

```latex
\documentclass{article}
\begin{document}
Statement \#1:
50\% of \$100 makes \$50.
More special symbols are \&, \_, \{ and \}.
\end{document}
```
```latex
\documentclass{article}
\begin{document}
Text can be \emph{emphasized}.
Besides being \textit{italic} words could be \textbf{bold},
\textsl{slanted} or typeset in \textsc{Small Caps}.
Such commands can be \textit{\textbf{nested}}.
\emph{See how \emph{emphasizing} looks when nested.}
\end{document}
```
```latex
\documentclass{article}
\begin{document}
\section{\textsf{\LaTeX\ resources on the internet}}
The best place for downloading LaTeX related software is CTAN.
Its address is \texttt{http://www.ctan.org}.
\end{document}
```
```latex
\documentclass{article}
\begin{document}
{\sffamily
Text can be {\em emphasized}.
Besides being {\itshape italic} words could be {\bfseries bold},
{\slshape slanted} or typeset in {\scshape Small Caps}.
Such commands can be {\itshape\bfseries nested}.}
{\em See how {\em emphasizing} looks when nested.}
\end{document}
```
```latex
\documentclass{article}
\begin{document}
\noindent\tiny We \scriptsize start \footnotesize \small small,
\normalsize get \large big \Large and \LARGE bigger,
\huge huge and \Huge gigantic!
\end{document}
```
```latex
\documentclass{article}
\newcommand{\keyword}[1]{\textbf{#1}}
\begin{document}
\keyword{Grouping} by curly braces limits the
\keyword{scope} of \keyword{declarations}.
\end{document}
```
```latex
\documentclass{article}
\begin{document}
Text line
\quad\parbox[b]{1.8cm}{this parbox is aligned at its bottom line}
\quad\parbox{1.5cm}{center-aligned parbox}
\quad\parbox[t]{2cm}{another parbox aligned at its top line}
\end{document}
```
```latex
\documentclass{article}
\begin{document}
\emph{Annabel Lee}\\
It was many and many a year ago,\\
In a kingdom by the sea,\\
That a maiden there lived whom you may know\\
By the name of Annabel Lee
\end{document}
```

```latex
\documentclass{article}
\usepackage{url}
\begin{document}
\noindent This is the beginning of a poem
by Edgar Allan Poe:
\begin{center}
\emph{Annabel Lee}
\end{center}
\begin{center}
It was many and many a year ago,\\
In a kingdom by the sea,\\
That a maiden there lived whom you may know\\
By the name of Annabel Lee
\end{center}
The complete poem can be read on
\url{http://www.online-literature.com/poe/576/}.
\end{document}
```
```latex
\documentclass{article}
\begin{document}
Niels Bohr said: ``An expert is a person who has made
all the mistakes that can be made in a very narrow field.''
Albert Einstein said:
\begin{quote}
Anyone who has never made a mistake has never tried anything new.
\end{quote}
Errors are inevitable. So, let's be brave trying something new.
\end{document}
```
```latex
\documentclass{article}
\usepackage{url}
\begin{document}
The authors of the CTAN team listed ten good reasons
for using \TeX. Among them are:
\begin{quotation}
\TeX\ has the best output. What you end with,
the symbols on the page, is as useable, and beautiful,
as a non-professional can produce.
\TeX\ knows typesetting. As those plain text samples
show, \TeX's has more sophisticated typographical algorithms
such as those for making paragraphs and for hyphenating.
\TeX\ is fast. On today's machines \TeX\ is very fast.
It is easy on memory and disk space, too.
\TeX\ is stable. It is in wide use, with a long history.
It has been tested by millions of users, on demanding input.
It will never eat your document. Never.
\end{quotation}
The original text can be found on
\url{ http://www.ctan.org/what_is_tex.html}.
\end{document}
```

```latex
\documentclass[a4paper,12pt]{book}
\usepackage[english]{babel}
\usepackage{blindtext}
\begin{document}
\chapter{Exploring the page layout}
In this chapter we will study the layout of pages.
\section{Some filler text}
\blindtext
\section{A lot more filler text}
More dummy text will follow.
\subsection{Plenty of filler text}
\blindtext[10]
\end{document}
```

```latex
\documentclass[a4paper,12pt]{book}
\usepackage[english]{babel}
\usepackage{blindtext}
\usepackage[a4paper, inner=1.5cm, outer=3cm, top=2cm,
bottom=3cm, bindingoffset=1cm]{geometry}
\begin{document}
\tableofcontents
\chapter{Exploring the page layout}
In this chapter we will study the layout of pages.
\section{Some filler text}
\blindtext
\section{A lot more filler text}
More dummy text will follow.
\subsection{Plenty of filler text}
\blindtext[10]
\end{document}
```

```latex
\documentclass[a4paper,12pt]{book}
\usepackage[english]{babel}
\usepackage{blindtext}
\usepackage[a4paper, inner=1.5cm, outer=3cm, top=2cm,
bottom=3cm, bindingoffset=1cm]{geometry}
\begin{document}
\tableofcontents
\chapter[Page layout]{Exploring the page layout}
In this chapter we will study the layout of pages.
\section[Filler text]{Some filler text}
\blindtext
\section[More]{A lot more filler text}
More blindtext will follow.
\subsection[Plenty]{Plenty of filler text}
\blindtext[10]
\end{document}
```

```latex
\documentclass[a4paper,12pt]{book}
\usepackage[english]{babel}
\usepackage{blindtext}
\usepackage{fancyhdr}
\fancyhf{}
\fancyhead[LE]{\leftmark}
\fancyhead[RO]{\nouppercase{\rightmark}}
\fancyfoot[LE,RO]{\thepage}
\pagestyle{fancy}
\begin{document}
	\chapter{Exploring the page layout}
	In this chapter we will study the layout of pages.
	\section{Some filler text}
\blindtext
\section{A lot more filler text}
More dummy text will follow.
\subsection{Plenty of filler text}
\blindtext[10]
\end{document}	
```

```latex
\documentclass{article}
\begin{document}
\section*{Useful packages}
LaTeX provides several packages for designing the layout:
\begin{itemize}
\item geometry
\item typearea
\item fancyhdr
\item scrpage2
\item setspace
\end{itemize}
\end{document}

```

```latex
\begin{itemize}
\item Page layout
\begin{itemize}
\item geometry
\item typearea
\end{itemize}
\item Headers and footers
\begin{itemize}
\item fancyhdr
\item scrpage2
\end{itemize}
\item Line spacing
\begin{itemize}
\item setspace
\end{itemize}
\end{itemize}
```


```latex
\documentclass{article}
\begin{document}
\begin{enumerate}
\item State the paper size by an option to the document class
\item Determine the margin dimensions using one of these
packages:
\begin{itemize}
\item geometry
\item typearea
\end{itemize}
\item Customize header and footer by one of these packages:
\begin{itemize}
\item fancyhdr
\item scrpage2
\end{itemize}
\item Adjust the line spacing for the whole document
\begin{itemize}
\item by using the setspace package
\item or by the command \verb|\linespread{factor}|
\end{itemize}
\end{enumerate}
\end{document}
```


